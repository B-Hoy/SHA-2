// Based on https://nvlpubs.nist.gov/nistpubs/FIPS/NIST.FIPS.180-4.pdf
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#define MAND_PADDING 17 // mandatory 0x80 byte and 128-bit length
#define r_rot(x, n) ((x >> n) | (x << ((sizeof(x) * 8) - n)))
#define sig0(x) (r_rot(x, 28) ^ r_rot(x, 34) ^ r_rot(x, 39))
#define sig1(x) (r_rot(x, 14) ^ r_rot(x, 18) ^ r_rot(x, 41))
#define gam0(x) (r_rot(x, 1) ^ r_rot(x, 8) ^ (x >> 7))
#define gam1(x) (r_rot(x, 19) ^ r_rot(x, 61) ^ (x >> 6))
#define ch(x, y, z) ((x & y) ^ ((~x) & z))
#define maj(x, y, z) ((x & y) ^ (x & z) ^ (y & z))
#define end_test() (*(char*)&(uint_fast16_t){0x01}) // 1 == LE, 0 == BE
uint64_t u64tobe(uint64_t input){
	if (end_test()){
		return  ((input << 56) & 0xff00000000000000)|
				((input << 40) & 0x00ff000000000000)|
				((input << 24) & 0x0000ff0000000000)|
				((input << 8)  & 0x000000ff00000000)|
				((input >> 8)  & 0x00000000ff000000)|
				((input >> 24) & 0x0000000000ff0000)|
				((input >> 40) & 0x000000000000ff00)|
				((input >> 56) & 0x00000000000000ff);
	}else{
		return input;
	}
}
int main(void){
	uint64_t h[8] = {0x22312194fc2bf72c, 0x9f555fa3c84c64c2,
		0x2393b86b6f53b151, 0x963877195940eabd, 0x96283ee2a88effe3,
		0xbe5e1e2553863992, 0x2b0199fc2c85b8aa, 0x0eb72ddc81c52ca2};
	uint64_t k[80] = {
		0x428a2f98d728ae22, 0x7137449123ef65cd, 0xb5c0fbcfec4d3b2f,
		0xe9b5dba58189dbbc,	0x3956c25bf348b538, 0x59f111f1b605d019,
		0x923f82a4af194f9b, 0xab1c5ed5da6d8118, 0xd807aa98a3030242,
		0x12835b0145706fbe, 0x243185be4ee4b28c, 0x550c7dc3d5ffb4e2,
		0x72be5d74f27b896f, 0x80deb1fe3b1696b1, 0x9bdc06a725c71235,
		0xc19bf174cf692694, 0xe49b69c19ef14ad2, 0xefbe4786384f25e3,
		0x0fc19dc68b8cd5b5, 0x240ca1cc77ac9c65,	0x2de92c6f592b0275,
		0x4a7484aa6ea6e483, 0x5cb0a9dcbd41fbd4, 0x76f988da831153b5,
		0x983e5152ee66dfab, 0xa831c66d2db43210, 0xb00327c898fb213f,
		0xbf597fc7beef0ee4,	0xc6e00bf33da88fc2, 0xd5a79147930aa725,
		0x06ca6351e003826f, 0x142929670a0e6e70,	0x27b70a8546d22ffc,
		0x2e1b21385c26c926, 0x4d2c6dfc5ac42aed, 0x53380d139d95b3df,
		0x650a73548baf63de, 0x766a0abb3c77b2a8, 0x81c2c92e47edaee6,
		0x92722c851482353b,	0xa2bfe8a14cf10364, 0xa81a664bbc423001,
		0xc24b8b70d0f89791, 0xc76c51a30654be30,	0xd192e819d6ef5218,
		0xd69906245565a910, 0xf40e35855771202a, 0x106aa07032bbd1b8,
		0x19a4c116b8d2d0c8, 0x1e376c085141ab53, 0x2748774cdf8eeb99,
		0x34b0bcb5e19b48a8,	0x391c0cb3c5c95a63, 0x4ed8aa4ae3418acb,
		0x5b9cca4f7763e373, 0x682e6ff3d6b2b8a3,	0x748f82ee5defb2fc,
		0x78a5636f43172f60, 0x84c87814a1f0ab72, 0x8cc702081a6439ec,
		0x90befffa23631e28, 0xa4506cebde82bde9, 0xbef9a3f7b2c67915,
		0xc67178f2e372532b,	0xca273eceea26619c, 0xd186b8c721c0c207,
		0xeada7dd6cde0eb1e, 0xf57d4f7fee6ed178,	0x06f067aa72176fba,
		0x0a637dc5a2c898a6, 0x113f9804bef90dae, 0x1b710b35131c471b,
		0x28db77f523047d84, 0x32caab7b40c72493, 0x3c9ebe0a15c9bebc,
		0x431d67c49c100d4c,	0x4cc5d4becb3e42b6, 0x597f299cfc657e2a,
		0x5fcb6fab3ad6faec, 0x6c44198c4a475817};
	FILE* file = fopen("testfile", "rb");
	fseek(file, 0, SEEK_END);
	unsigned long file_size = ftell(file);
	fseek(file, 0, SEEK_SET);
	uint_fast8_t message_padding = 128 - ((file_size + MAND_PADDING) % 128);
	unsigned long total_len = file_size + message_padding + MAND_PADDING; 
	unsigned char* message = calloc(total_len, 1);
	fread(message, 1, file_size, file);
	message[file_size] = 0x80;
	uint64_t t1, t2, w[80], ah[8], pad_ilen = u64tobe(file_size * 8);
	memcpy(message + total_len - 8, (char*)&pad_ilen, 8);
	for (size_t chunk = 0; chunk < total_len; chunk += 128){
		for (uint_fast8_t i = 0; i < 16; i++){
			w[i] = u64tobe(*(uint64_t*)(message + chunk + (i * 8)));
		}
		for (uint_fast8_t i = 16; i < 80; i++){
			w[i] = gam1(w[i - 2]) + w[i - 7] + gam0(w[i - 15]) + w[i - 16];
		}
		memcpy(ah, h, sizeof(h));
		for (uint_fast8_t i = 0; i < 80; i++){
			t1 = ah[7] + sig1(ah[4]) + ch(ah[4], ah[5], ah[6]) + k[i] + w[i];
			t2 = sig0(ah[0]) + maj(ah[0], ah[1], ah[2]);
			ah[7] = ah[6];
			ah[6] = ah[5];
			ah[5] = ah[4];
			ah[4] = ah[3] + t1;
			ah[3] = ah[2];
			ah[2] = ah[1];
			ah[1] = ah[0];
			ah[0] = t1 + t2;
		}
		for (uint_fast8_t i = 0; i < 8; i++){
			h[i] += ah[i];
		}
	}
	printf("%016lx%016lx%016lx%016lx\n", 
		h[0], h[1], h[2], h[3]);
	free(message);
	return 0;
}

// Based on https://nvlpubs.nist.gov/nistpubs/FIPS/NIST.FIPS.180-4.pdf
#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#define MAND_PADDING 9 // mandatory 0x80 byte and 64-bit length
#define r_rot(x, n) ((x >> n) | (x << ((sizeof(x) * 8) - n)))
#define sig0(x) (r_rot(x, 2) ^ r_rot(x, 13) ^ r_rot(x, 22))
#define sig1(x) (r_rot(x, 6) ^ r_rot(x, 11) ^ r_rot(x, 25))
#define gam0(x) (r_rot(x, 7) ^ r_rot(x, 18) ^ (x >> 3))
#define gam1(x) (r_rot(x, 17) ^ r_rot(x, 19) ^ (x >> 10))
#define ch(x, y, z) ((x & y) ^ ((~x) & z))
#define maj(x, y, z) ((x & y) ^ (x & z) ^ (y & z))
#define end_test() (*(char*)&(uint_fast16_t){0x01}) // 1 == LE, 0 == BE
uint64_t u64tobe(uint64_t input){
	if (end_test()){
		return  ((input << 56) & 0xff00000000000000)|
				((input << 40) & 0x00ff000000000000)|
				((input << 24) & 0x0000ff0000000000)|
				((input << 8)  & 0x000000ff00000000)|
				((input >> 8)  & 0x00000000ff000000)|
				((input >> 24) & 0x0000000000ff0000)|
				((input >> 40) & 0x000000000000ff00)|
				((input >> 56) & 0x00000000000000ff);
	}else{
		return input;
	}
}
uint32_t u32tobe(uint32_t input){
	if (end_test()){
		return	((input << 24) & 0xff000000)|
				((input << 8)  & 0x00ff0000)|
				((input >> 8)  & 0x0000ff00)|
				((input >> 24) & 0x000000ff);
	}else{
		return input;
	}
}
int main(void){
	uint32_t h[8] = {0x6a09e667, 0xbb67ae85, 0x3c6ef372, 0xa54ff53a,
		0x510e527f, 0x9b05688c, 0x1f83d9ab, 0x5be0cd19};
	uint32_t k[64] = {0x428a2f98, 0x71374491, 0xb5c0fbcf, 0xe9b5dba5,
		0x3956c25b, 0x59f111f1, 0x923f82a4, 0xab1c5ed5, 0xd807aa98, 0x12835b01,
		0x243185be, 0x550c7dc3, 0x72be5d74, 0x80deb1fe, 0x9bdc06a7, 0xc19bf174,
		0xe49b69c1, 0xefbe4786, 0x0fc19dc6, 0x240ca1cc, 0x2de92c6f, 0x4a7484aa,
		0x5cb0a9dc, 0x76f988da, 0x983e5152, 0xa831c66d, 0xb00327c8, 0xbf597fc7,
		0xc6e00bf3, 0xd5a79147, 0x06ca6351, 0x14292967, 0x27b70a85, 0x2e1b2138,
		0x4d2c6dfc, 0x53380d13, 0x650a7354, 0x766a0abb, 0x81c2c92e, 0x92722c85,
		0xa2bfe8a1, 0xa81a664b, 0xc24b8b70, 0xc76c51a3, 0xd192e819, 0xd6990624,
		0xf40e3585, 0x106aa070, 0x19a4c116, 0x1e376c08, 0x2748774c, 0x34b0bcb5,
		0x391c0cb3, 0x4ed8aa4a, 0x5b9cca4f, 0x682e6ff3, 0x748f82ee, 0x78a5636f,
		0x84c87814, 0x8cc70208, 0x90befffa, 0xa4506ceb, 0xbef9a3f7, 0xc67178f2
		};
	FILE* file = fopen("testfile", "rb");
	fseek(file, 0, SEEK_END);
	unsigned long file_size = ftell(file);
	fseek(file, 0, SEEK_SET);
	uint_fast8_t message_padding = 64 - ((file_size + MAND_PADDING) % 64);
	unsigned long total_len = file_size + message_padding + MAND_PADDING; 
	unsigned char* message = calloc(total_len, 1);
	fread(message, 1, file_size, file);
	message[file_size] = 0x80;
	uint64_t pad_ilen = u64tobe(file_size * 8);
	memcpy(message + total_len - 8, (char*)&pad_ilen, 8);
	// Preprocessing done
	uint32_t w[64], ah[8], t1, t2;
	for (size_t chunk = 0; chunk < total_len; chunk += 64){
		// 1. Prepare the message schedule
		for (uint_fast8_t i = 0; i < 16; i++){
			w[i] = u32tobe(*(uint32_t*)(message + chunk + (i * 4)));
		}
		for (uint_fast8_t i = 16; i < 64; i++){
			w[i] = gam1(w[i - 2]) + w[i - 7] + gam0(w[i - 15]) + w[i - 16];
		}
		// 2. Initialise the 8 working variables
		memcpy(ah, h, sizeof(h));
		// 3.
		for (uint_fast8_t i = 0; i < 64; i++){
			t1 = ah[7] + sig1(ah[4]) + ch(ah[4], ah[5], ah[6]) + k[i] + w[i];
			t2 = sig0(ah[0]) + maj(ah[0], ah[1], ah[2]);
			ah[7] = ah[6];
			ah[6] = ah[5];
			ah[5] = ah[4];
			ah[4] = ah[3] + t1;
			ah[3] = ah[2];
			ah[2] = ah[1];
			ah[1] = ah[0];
			ah[0] = t1 + t2;
		}
		// 4.
		for (uint_fast8_t i = 0; i < 8; i++){
			h[i] += ah[i];
		}
	}
	printf("%08x%08x%08x%08x%08x%08x%08x%08x\n", 
		h[0], h[1], h[2], h[3], h[4], h[5], h[6], h[7]);
	free(message);
	return 0;
}
